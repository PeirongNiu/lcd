package lcd;

import java.util.ArrayList;
import java.util.List;

public class Lcd {
    public static void main(String[] args) {
        char[][] number;
        List<char[][]> template = new ArrayList<>();

        int size;
        if (args.length > 1)
            size = Integer.parseInt(args[1]);
        else
            size = 1;

        int row = 5 + (size - 1) * 2;
        int col = 3 + size - 1;

        number = new char[row][col];
        number[0][1] = '-';
        number[1][col-1] = '|';
        number[row-2][col-1] = '|';
        number[row-1][1] = '-';
        number[row-2][0] = '|';
        number[1][0] = '|';
        getTemplate(number, size);
        number = bigSizeJudege(number, size, row, col);
        template.add(number);

        number = new char[row][col];
        number[1][col-1] = '|';
        number[row-2][col-1] = '|';
        getTemplate(number, size);
        number = bigSizeJudege(number, size, row, col);
        template.add(number);

        number = new char[row][col];
        number[0][1] = '-';
        number[1][col-1] = '|';
        number[row/2][1] = '-';
        number[row-2][0] = '|';
        number[row-1][1] = '-';
        getTemplate(number, size);
        number = bigSizeJudege(number, size, row, col);
        template.add(number);

        number = new char[row][col];
        number[0][1] = '-';
        number[1][col-1] = '|';
        number[row/2][1] = '-';
        number[row-2][col-1] = '|';
        number[row-1][1] = '-';
        getTemplate(number, size);
        number = bigSizeJudege(number, size, row, col);
        template.add(number);

        number = new char[row][col];
        number[1][0] = '|';
        number[row/2][1] = '-';
        number[1][col-1] = '|';
        number[row-2][col-1] = '|';
        getTemplate(number, size);
        number = bigSizeJudege(number, size, row, col);
        template.add(number);

        number = new char[row][col];
        number[0][1] = '-';
        number[1][0] = '|';
        number[row/2][1] = '-';
        number[row-2][col-1] = '|';
        number[row-1][1] = '-';
        getTemplate(number, size);
        number = bigSizeJudege(number, size, row, col);
        template.add(number);

        number = new char[row][col];
        number[0][1] = '-';
        number[1][0] = '|';
        number[row/2][1] = '-';
        number[row-2][col-1] = '|';
        number[row-1][1] = '-';
        number[row-2][0] = '|';
        getTemplate(number, size);
        number = bigSizeJudege(number, size, row, col);
        template.add(number);

        number = new char[row][col];
        number[0][1] = '-';
        number[1][col-1] = '|';
        number[row-2][col-1] = '|';
        getTemplate(number, size);
        number = bigSizeJudege(number, size, row, col);
        template.add(number);

        number = new char[row][col];
        number[0][1] = '-';
        number[1][0] = '|';
        number[1][col-1] = '|';
        number[row/2][1] = '-';
        number[row-2][0] = '|';
        number[row-2][col-1] = '|';
        number[row-1][1] = '-';
        getTemplate(number, size);
        number = bigSizeJudege(number, size, row, col);
        template.add(number);

        number = new char[row][col];
        number[0][1] = '-';
        number[1][0] = '|';
        number[1][col-1] = '|';
        number[row/2][1] = '-';
        number[row-2][col-1] = '|';
        number[row-1][1] = '-';
        getTemplate(number, size);
        number = bigSizeJudege(number, size, row, col);
        template.add(number);


        input(args[0], template, row, col);


    }

    private static void input(String arg, List<char[][]> template, int row, int col) {
        String input = arg;
        for (int t=0; t<row; t++){

            for (int i = 0; i< arg.length(); i++){
                int result = input.charAt(i)-'0';
                for (int j=0; j<col; j++){
                    System.out.print(template.get(result)[t][j]);
                }
            }
            System.out.println();
        }
    }

    private static char[][] bigSizeJudege(char[][] number, int size, int row, int col) {
        int temp1 = 1, temp2 = 1, temp3 = row-2, temp4 = 1,temp5 = 1, temp6 = 1, temp7 = row-2;
        for (int i=0; i<size-1; i++){

            if (number[0][1] == '-'){
                number[0][++temp1] = '-';
            }
            if (number[1][0] == '|'){
                number[++temp2][0] = '|';
            }
            if (number[row-2][0] == '|'){
                number[--temp3][0] = '|';
            }
            if (number[row-1][1] == '-'){
                number[row-1][++temp4] = '-';
            }
            if (number[row/2][1] == '-'){
                number[row/2][++temp5] = '-';
            }
            if (number[1][col-1] == '|'){
                number[++temp6][col-1] = '|';
            }
            if (number[row-2][col-1] == '|'){
                number[--temp7][col-1] = '|';
            }
        }
        return number;
    }

    private static char[][] getTemplate(char[][] number, int size) {
        for (int i = 0; i < 5+(size-1)*2; i++) {
            for (int j = 0; j < 3+size-1; j++) {
                if (number[i][j] == '\0') {
                    number[i][j] = ' ';
                }
            }
        }
        return number;
    }
}
